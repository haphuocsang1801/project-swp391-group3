var synth = window.speechSynthesis
var utterance = new SpeechSynthesisUtterance("Hello World")
if ('speechSynthesis' in window) {
    var synthesis = window.speechSynthesis;
     // Get the first `en` language voice in the list
    var voice = synthesis.getVoices().filter(function (voice) {
        return voice.lang === 'en';
    })[0];

    // Create an utterance object
    var utterance = new SpeechSynthesisUtterance('Hello World');

    // Set utterance properties
    utterance.voice = voice;
    utterance.pitch = 1.5;
    utterance.rate = 1.25;
    utterance.volume = 0.8;
    
} else {
    console.log('not SUpported')
}

window.onload = () => {
    const icons = document.getElementsByClassName("audio-icon");

    for (let i = 0; i < icons.length; i++) {
        const element = icons[i];
        element.addEventListener('click', () =>{
            utterance.text = "teacher";
            synthesis.speak(utterance);
            
        });
    }
    
    const btn = document.querySelector(".continue-button");
    const cards = document.querySelectorAll(".thefront");
    for (let i = 0; i < cards.length; i++) {
        const element = cards[i];
        element.addEventListener('click', (e) =>{
            btn.classList.add("active");
            enableContinueBtn();

        });
    }

    const input_box = document.querySelector("input.textbox");
    input_box.addEventListener('keyup', (e) => {
        if (input_box.value != "")
        {
            btn.classList.add("active");
        } else{
            btn.classList.remove("active");
        }
    });

    const fill_in_word_box = document.querySelector("input.fill-in-the-word")
    fill_in_word_box.addEventListener('keyup', (e) => {
        if (fill_in_word_box.value != "")
        {
            btn.classList.add("active");
        } else{
            btn.classList.remove("active");
        }
    });

    
}

function enableContinueBtn(){
    const questions = document.querySelectorAll(".div-learn-content");
    const continuebtns = document.querySelectorAll(".continue-button.active");
    const div_answer = document.getElementById("div-answer");
    const answer = document.getElementById("answer");
    var question_num = 1;
    continuebtns[0].addEventListener('click', () =>{
        if(continuebtns[0].classList.contains("active") && !document.querySelector("[data-question-number='2']").classList.contains("active"))
        {
            const firstelement = questions[question_num-1];
            const nextelement = questions[question_num];
            let temp = question_num++;
            firstelement.classList.remove("active");
            nextelement.classList.add("active");
            continuebtns[0].classList.remove("active");
        }
        if(continuebtns[0].classList.contains("active") && document.querySelector("[data-question-number='2']").classList.contains("active") 
            && !div_answer.classList.contains("correct") && !div_answer.classList.contains("incorrect"))
        {
            const inputValue = document.getElementById("answer").value;
            console.log(inputValue);
            if (inputValue == "teacher")
            {
                div_answer.classList.add("correct")
            } else{
                div_answer.classList.add("incorrect");
            }
        } else if(continuebtns[0].classList.contains("active") && (div_answer.classList.contains("correct") || div_answer.classList.contains("incorrect")))
        {
            console.log("hello");
            const firstelement = questions[question_num-1];
            const nextelement = questions[question_num];
            let temp = question_num++;
            firstelement.classList.remove("active");
            nextelement.classList.add("active");
            continuebtns[0].classList.remove("active");
            if(div_answer.classList.contains("correct"))
            {
                div_answer.classList.remove("correct");
            } else{
                div_answer.classList.remove("incorrect");
            }
        }
    });
}


 