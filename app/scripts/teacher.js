const dropdownSelect = document.querySelectorAll(".dropdown__select");
const chapterItemsInner = document.querySelectorAll(".chapter-item-group");
const dropdowContent = document.querySelectorAll(".dropdown-content");
const chapterItems = document.querySelectorAll(".chapter-item");
const dropdown = document.querySelectorAll(".dropdown");

//handle text content dropdown selected
dropdown.forEach((elem) => {
    const dropdownItems = elem.querySelectorAll(".dropdown__item");
    const dropdowSelected = elem.querySelector(".dropdown__selected");
    dropdownItems.forEach((item) =>
        item.addEventListener("click", function (e) {
            const text = e.target.querySelector(".dropdown__text").textContent;
            console.log(text);
            dropdowSelected.textContent = text;
            handleDropdownSelect();
        })
    );
});

//handle dropdown
dropdownSelect.forEach((item) =>
    item.addEventListener("click", handleDropdownSelect)
);
//handle dropdown chapter
[...chapterItemsInner].forEach((item) => {
    item.addEventListener("click", handleDropdowChapter);
});
//change text on dropdownn item

function handleDropdownItems(e) {
    const text = e.target.querySelector(".dropdown__text").textContent;

    dropdowSelected.textContent = text;
    handleDropdownSelect();
}

function handleDropdownSelect() {
    dropdowContent.forEach((item) => {
        item.classList.toggle("dropdown-active");
        item.style.height = `${item.scrollHeight}px`;
        if (!item.classList.contains("dropdown-active")) {
            item.style.height = `0px`;
        }
        const icon = document.querySelectorAll(".dropdown__caret");
        icon.forEach((elem) => {
            elem.classList.toggle("icon-active");
            elem.classList.toggle("icon-remove");
        });
    });
}

function handleDropdowChapter(e) {
    const content = e.target.nextElementSibling;
    content.classList.toggle("is-active");
    const icon = e.target.querySelector("i");
    icon.classList.toggle("icon-active");
    icon.classList.toggle("icon-remove");
}
