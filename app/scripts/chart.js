//Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([
        ['Level', 'Words', { role: 'style' } ],
        ['Level 1', 3, 'color: #EB5757'],
        ['Level 2', 15, 'color: #FFCB09'],
        ['Level 3', 7, 'color: #56CBF2'],
        ['Level 4', 4, 'color: #2F7FEC'],
        ['Level 5', 2, 'color: #213682']
    ]);
    // Set chart options
    var options = { 'width':750,
                    'height':300,
                    'legend':'none',
                    vAxis:{
                        textPosition: 'none',
                        gridlines: {
                            color: 'transparent'
                                }
                        },
                    };
    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(data, options);
    }