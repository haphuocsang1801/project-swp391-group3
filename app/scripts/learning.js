const openModalButtons = document.querySelector(".course-select");
const closeModalButtons = document.querySelector(".close-button");
const modal = document.querySelector(".courseselection");
const overlay = document.querySelector(".courseselectOverlay");

openModalButtons.addEventListener("click", (e) => {
    if (modal == null) return;
    modal.classList.add("active");
    overlay.classList.add("active");
});

closeModalButtons.addEventListener("click", (e) => {
    if (modal == null) return;
    modal.classList.remove("active");
    overlay.classList.remove("active");
});

document.body.addEventListener("click", (e) => {
    if (e.target.matches(".courseselectOverlay")) {
        overlay.classList.remove("active");
        modal.classList.remove("active");
    }
});

window.onload = () => {
    const tab_switchers = document.querySelectorAll("[data-switcher]");
    const level_switchers = document.querySelectorAll("[data-switcher-2]");
    for (let i = 0; i < tab_switchers.length; i++) {
        const tab_switcher = tab_switchers[i];
        const page_id = tab_switcher.dataset.tab;
        tab_switcher.addEventListener("click", () => {
            document
                .querySelector(".icons-icon.is-active")
                .classList.remove("is-active");
            tab_switcher.classList.add("is-active");
            switchPage(page_id);
        });
    }
    for (let i = 0; i < level_switchers.length; i++) {
        const level_switcher = level_switchers[i];
        const level = level_switcher.dataset.level;
        level_switcher.addEventListener("click", () => {
            document
                .querySelector(".level-bar-level.is-active")
                .classList.remove("is-active");
            level_switcher.classList.add("is-active");

            switchLevel(level);
        });
    }
};

function switchPage(page_id) {
    const currentPage = document.querySelector(".pages .page.is-active");
    currentPage.classList.remove("is-active");

    const nextPage = document.querySelector(
        `.pages .page[data-page="${page_id}"]`
    );
    nextPage.classList.add("is-active");
}

function switchLevel(level) {
    const currentPage = document.querySelector(".wordlists-level.is-active");
    currentPage.classList.remove("is-active");

    const nextPage = document.querySelector(
        `.wordlists-level[data-word-level="${level}"]`
    );
    nextPage.classList.add("is-active");
}
